package taller.interfaz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import javax.swing.JOptionPane;

import org.json.JSONObject;
public class ArbolCLI implements IReconstructorArbol {

	public class Nodo 
	{
		public String v;
		Nodo izq, der;

		public Nodo(String val) 
		{
			v = val;
			izq = der = null;
		}
		public Nodo darIzq()
		{
			return izq;
		}
		public Nodo darDer()
		{
			return der;
		}
	}

	private String[] preOrden;
	private String[] inOrden;
	public Nodo raiz;
	private List<Nodo> nodos;
	private int preIndex;
	private Scanner in;

	public ArbolCLI()
	{
		in = new Scanner(System.in);
		nodos = new ArrayList<Nodo>();
		preIndex = 0;
	}

	public void mainMenu()
	{
		boolean finish = false;
		while(!finish)
		{	
			Screen.clear();
			System.out.println("------------------------------------------");
			System.out.println("-                                        -");
			System.out.println("-           Siembra de árboles           -");
			System.out.println("-                                        -");
			System.out.println("------------------------------------------");
			System.out.println("EL sistema para la plantación de árboles binarios\n");

			System.out.println("Menú principal:");
			System.out.println("-----------------");
			System.out.println("1. Cargar archivo con semillas");
			System.out.println("2. Salir");
			System.out.print("\nSeleccione una opción: ");
			int opt1 = Integer.parseInt(in.next());
			switch(opt1)
			{
			case 1:
				recibirArchivo();
				finish = true;
				break;
			case 2:
				finish = true;
				break;
			default:
				break;
			}
		}
	}

	public void recibirArchivo()
	{
		boolean finish = false;
		while(!finish)
		{
			Screen.clear();
			System.out.println("Recuerde que el archivo a cargar");
			System.out.println("debe ser un archivo properties");
			System.out.println("que tenga la propiedad in-orden,");
			System.out.println("la propiedad pre-orden (donde los ");
			System.out.println("elementos estén separados por comas) y");
			System.out.println("que esté guardado en la carpeta data.");
			System.out.println("");
			System.out.println("Introduzca el nombre del archivo:");
			System.out.println("----------------------------------------------------");

			// TODO Leer el archivo .properties
			String direccion = in.next();

			try {
				// TODO cargarArchivo
				cargarArchivo(direccion);
				// TODO Reconstruir árbol 
				reconstruir();
				crearArchivo("arbolPlantado.json");

				System.out.println("Ha plantado el árbol con éxito!\nPara verlo, dirijase a /data/arbolPlantado.json");
				System.out.println("Nota: ejecute Refresh (Clic derecho - Refresh) sobre la carpeta /data/ para actualizar y visualizar el archivo JSON");
				System.out.println("Presione 1 para salir");
				in.next();
				finish=true;

			} catch (Exception e) {
				System.out.println("Hubo un problema cargando el archivo:");
				System.out.println(e.getMessage());
				e.printStackTrace();

			}
		}
	}

	@Override
	public void cargarArchivo(String nombre) throws IOException {
		FileInputStream fis;
		fis = new FileInputStream( new File( "./data/"+nombre));
		Properties propiedades = new Properties( );
		propiedades.load( fis );

		preOrden= (propiedades.getProperty("preorden")).split(",");
		inOrden = (propiedades.getProperty("inorden")).split(",");
		fis.close();
	}

	@Override
	public void crearArchivo(String nom) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		File f = new File(("./data/"+nom));
		if (!f.exists()){
			try {
				f.createNewFile();
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		JSONObject obj = new JSONObject();
		obj = pasarJson(raiz, nodos);

		try (FileWriter file = new FileWriter(("./data/"+nom))) {
			file.write(obj.toString());
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public JSONObject pasarJson(Nodo node, List<Nodo> otros) {
		JSONObject obj = new JSONObject();
		obj.put("Valor", node.v);
		List<JSONObject> hijos = new ArrayList<JSONObject>();
		ArrayList<Nodo> a = new ArrayList<Nodo>();
		otros.remove(a);
		for (Nodo n: otros){
			if (esHijo(n, node) && n != null){
				a.add(n);
				hijos.add(pasarJson(n, otros));
			}
		}
		obj.put("Hijos", hijos);
		return obj;
	}
	
	public boolean esHijo (Nodo hijo, Nodo nodo)
	{
		return nodo.izq==hijo||nodo.der==hijo;
	}
	
	@Override
	public void reconstruir() {
		raiz = construirArbol(0, preOrden.length - 1);
		nodos.add(raiz);
	}
	
	public Nodo construirArbol(int ini, int fin) 
	{

		if (ini > fin) 
			return null;

		if (preIndex>preOrden.length-1)
			return null;
		Nodo nodo = new Nodo(preOrden[preIndex++]);

		if (ini == fin)
			return nodo;

		int inIndex = buscar(inOrden, ini, fin, nodo.v);
		nodo.izq = construirArbol(ini, inIndex - 1);
		nodos.add(nodo.izq);
		nodo.der = construirArbol(inIndex + 1, fin);
		nodos.add(nodo.der);

		return nodo;
	}
	
	public int buscar(String arr[], int ini, int fin, String val) 
	{
		int r = -1;
		for (int i = ini; i <= fin; i++) 
		{
			if (arr[i].equals(val)){
				r = i;
			}
		}
		if (r == -1){
			System.out.println(val);
		}
		return r;
	}
	
	public boolean esSubarbol(Nodo arbol, Nodo aBuscar) 
	{
		if (aBuscar == null || sonIguales(arbol, aBuscar)) 
			return true;

		else if (arbol == null)
			return false;


		return esSubarbol(arbol.izq, aBuscar)|| esSubarbol(arbol.der, aBuscar);
	}
	
	public boolean sonIguales(Nodo raiz1, Nodo raiz2) 
	{
		if (raiz1 == null && raiz2 == null)
			return true;

		if (raiz1 == null || raiz2 == null)
			return false;

		return (raiz1.v == raiz2.v && sonIguales(raiz1.izq, raiz2.izq)&& sonIguales(raiz1.der, raiz2.der));
	}
	
	public String[] darPreOrden()
	{
		return preOrden;
	}
	
	public String[] darInOrden()
	{
		return inOrden;
	}
	
	public Nodo darRaiz()
	{
		return raiz;
	}
} 