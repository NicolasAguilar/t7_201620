package taller.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import taller.interfaz.ArbolCLI;

public class testArbolCLI {


	ArbolCLI l = new ArbolCLI();
	public void SetUp()
	{
		try {
			l.cargarArchivo("caso1.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void SetUp2()
	{
		try {
			l.cargarArchivo("caso2.properties");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testCargarArchivo() {
		SetUp();
		assertEquals("A", l.darPreOrden()[0]);
		int size = l.darInOrden().length;
		assertEquals("C".toString(), l.darInOrden()[size-2].toString());
		SetUp2();
		assertEquals("J", l.darPreOrden()[0]);
		int tamnho = l.darInOrden().length;
		assertEquals("O".toString(), l.darInOrden()[tamnho-2].toString());
		SetUp2();
	}
	
	@Test
	public void testReconstruir()
	{
		SetUp();
		l.reconstruir();
		assertEquals("A", l.raiz.v);
		assertEquals("D", l.raiz.darIzq().darIzq().v);
		assertEquals("G", l.raiz.darDer().darDer().v);
		
		l = new ArbolCLI();
		SetUp2();
		l.reconstruir();
		assertEquals("J", l.raiz.v);
		assertEquals("L", l.raiz.darIzq().darDer().v);
		assertEquals("P", l.raiz.darDer().darDer().v);
	}
}
